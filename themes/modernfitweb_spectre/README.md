Modern Fit Web Theme
==========

Modern Fit Web Theme : Based on the latest Spectre CSS framework, using the flexbox-based grids to deliver a responsive and mobile-friendly layout, backward compatible with older browsers (IE 9).
## Combining CSS and JavaScript

This theme doesn't combine assets for performance reasons. To combine the stylesheets, replace the following lines in the default layout. When combining with this theme, we recommend enabling the config `enableAssetDeepHashing` in the file **config/cms.php**.

Uncombined stylesheets:

    <link href="{{ 'assets/css/vendor.css'|theme }}" rel="stylesheet">
    <link href="{{ 'assets/css/theme.css'|theme }}" rel="stylesheet">

Combined stylesheets:

    <link href="{{ [
        '@framework.extras',
        'assets/less/vendor.less',
        'assets/less/theme.less'
    ]|theme }}" rel="stylesheet">

> **Note**: October also includes an SCSS compiler, if you prefer.

Uncombined JavaScript:

    <script src="{{ 'assets/vendor/jquery.js'|theme }}"></script>
    <script src="{{ 'assets/vendor/bootstrap.js'|theme }}"></script>
    <script src="{{ 'assets/javascript/app.js'|theme }}"></script>
    {% framework extras %}

Combined JavaScript:

    <script src="{{ [
        '@jquery',
        '@framework',
        '@framework.extras',
        'assets/vendor/bootstrap.js',
        'assets/javascript/app.js'
    ]|theme }}"></script>

> **Important**: Make sure you keep the `{% styles %}` and `{% scripts %}` placeholder tags as these are used by plugins for injecting assets.
