<?php namespace Hambern\Company\Controllers;

use BackendMenu;
use Flash;
use Lang;
use Hambern\Company\Models\Tag;

use Config; /* FBR */
/**
 * Tags Back-end Controller
 */
class Tags extends Controller
{

    public $requiredPermissions = ['hambern.company.access_services'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Hambern.Company', 'company', 'tags');
        $this->addCss('/themes/'.Config::get('cms.activeTheme', false).'/assets/css/icons.data.svg.css'); /* FBR */
    }

    /**
     * Deleted checked tags.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $tagId) {
                if (!$tag = Tag::find($tagId)) continue;
                $tag->delete();
            }

            Flash::success(Lang::get('hambern.company::lang.tags.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('hambern.company::lang.tags.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
    
    /* FBR */
    public static function getIconOptions() {
        return \Fbr\BlogFeature\Models\BlogFeature::getBlogFeatureIconOptions();
    }
    public static function getIconHtmlContent($avalue = '')
    {
        $arr = self::getIconOptions();
        return empty($avalue) ? '' : (array_key_exists($avalue, $arr) ? $arr[$avalue] : '' );
    }
}
