<?php namespace Hambern\Company\Models;

/**
 * Link Model
 */
class Link extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'hambern_company_links';
    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'picture' => ['System\Models\File'],
    ];
    public $attachMany = [
        'pictures' => ['System\Models\File'],
    ];

    	/* FBR */
	public static function getIconOptions() {
		return \Fbr\BlogFeature\Models\BlogFeature::getBlogFeatureIconOptions();
	}
	public static function getIconHtmlContent($avalue = '')
	{
		$arr = self::getIconOptions();
		return empty($avalue) ? '' : (array_key_exists($avalue, $arr) ? $arr[$avalue] : '' );
	}
}