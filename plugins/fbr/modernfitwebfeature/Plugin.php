<?php namespace Fbr\ModernFitwebFeature;

use Backend;
use System\Classes\PluginBase;

/**
 * Map Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'ModernFitwebFeature',
            'description' => 'Features: Detect screen-size component on front-end & GetLastFacebookFeed',
            'author'      => 'ModernFitWeb.com',
            'icon'        => 'icon-eye'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Fbr\ModernFitwebFeature\Components\GetDimension' => 'getDimension',
            'Fbr\ModernFitwebFeature\Components\GetFacebookConnector' => 'getFacebookConnector',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [];
    }

}
