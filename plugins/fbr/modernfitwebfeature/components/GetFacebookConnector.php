<?php namespace Fbr\ModernFitwebFeature\Components;
use Lang;
//use Auth;
//use Event;
//use Flash;
//use Request;
//use Redirect;
//use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Response;
use Config; /* FBR */
use Session; // FBR

use Facebook;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookSession;
use FacebookAuthorizationException;

use Facebook\Authentication\AccessToken;

class GetFacebookConnector extends ComponentBase
{
    protected $appId                ='2019941584893590';
    protected $appSecret            = '06ac2c70372b39a06787dbfc236395c3';
    protected $pageId               = '326473987790429';
    protected $permanentAcessToken  = 'EAActIEyidpYBAONHaz9mhg0GUauFw2wUb8ahaE8CkNmqSVqXxhLNRmtxflt0DWPto9ZCBhkFlcVgDbp6KGJPrdgI6ifj0JAi2MJepkZBX5o7AxNnZCZBZBZC3FgitZAhZCI5ypWCKUuse2qQxZCpDudghYVK9R7EzrXTedpuPzEh7HXv7KG8lq6km';

    public function componentDetails()
    {
        return [
            'name'        => 'GetFacebookConnector - PHP SDK',
            'description' => 'PHP SDK v5.5, connet to FBgraph v2.10'
        ];
    }

    public function defineProperties()
    {
        return [

        ];
    }
    /* FBR */
  	public function onInit(){

        $this->appId ='2019941584893590';
        $this->appSecret = '06ac2c70372b39a06787dbfc236395c3';
        $this->pageId = '326473987790429';
        $this->permanentAcessToken = 'EAActIEyidpYBAONHaz9mhg0GUauFw2wUb8ahaE8CkNmqSVqXxhLNRmtxflt0DWPto9ZCBhkFlcVgDbp6KGJPrdgI6ifj0JAi2MJepkZBX5o7AxNnZCZBZBZC3FgitZAhZCI5ypWCKUuse2qQxZCpDudghYVK9R7EzrXTedpuPzEh7HXv7KG8lq6km';

  	}

    public function getFacebookLastPost()
    {

/*      use Cms\Classes\Theme;
      // FBR start autoload ///
      session_start();
      $themeActive = Theme::getActiveTheme()->getDirName();

      $pathAutoloader = base_path()
      .'/themes/'
      .$themeActive.'/assets/js/vendor/facebook/php-graph-sdk-5.5/src/Facebook/autoload.php';
*/
      session_start();
      //  /var/www/html/modernfitweb/plugins/fbr/modernfitwebfeature/components/vendor/facebook/php-graph-sdk-5.5/src/Facebook/autoload.php
      $pathAutoloader = dirname(__FILE__).'/vendor/facebook/php-graph-sdk-5.5/src/Facebook/';
      define('FACEBOOK_SDK_SRC_DIR', $pathAutoloader);
      $pathAutoloader .='autoload.php';


      /*$pathAutoloader = base_path()
        .'/themes/'
        .Config::get('cms.activeTheme', false)
        .'/assets/vendor/facebook/php-graph-sdk-5.5/src/Facebook/autoload.php';*/
      ///define('FACEBOOK_SDK_V4_SRC_DIR', $vendorDir .'/facebook-sdk-v5/');
      if(!is_file($pathAutoloader))
      {
          $result = [
              'status' => 'error',
              'message'   => "Could not find Facebook/autoload.php on requested: \n".$pathAutoloader
          ];
          $response = Response::json($result, 500)->send();
          exit;
      }
      require_once($pathAutoloader);
    //  use Facebook;

      $shareUrl = urlencode(post('shareUrl'));
      $shareUrl = 'http%3A%2F%2Fblogs.sitepointstatic.com%2Fexamples%2Ftech%2Fsocial-buttons%2Findex.html';
      $shareFacebook  = 'https://www.facebook.com/sharer/sharer.php?u='.$shareUrl;
      $shareGoogle    = 'https://plus.google.com/share?url='.$shareUrl;
      $shareTwitter   = 'https://twitter.com/intent/tweet?url='.$shareUrl
                          .'&text=ModernfitWeb.com+website+development+~+the+Opportunity+to+get+out+of+the+Line&hashtags=web,responsive,fast+loading+site';
      $shareLinkedIn = 'http://www.linkedin.com/shareArticle?mini=true&url='.$shareUrl
                          .'&source=ModernfitWeb.com&title=website+development+~+the+Opportunity+to+get+out+of+the+Line';

      // FBR end autoload \\\

      ///echo"<pre>";var_dump($pathAutoloader);echo"</pre>";exit;
      $fb = new Facebook\Facebook([
          'app_id' =>  $this->appId,
          'app_secret' => $this->appSecret,
          'default_graph_version' => 'v2.10',
          ]);
      $fb->setDefaultAccessToken($this->permanentAcessToken);
    /*
        // e.g. of how to Make a batch request
        $requestUserName = $fb->request('GET', '/me?fields=id,name');
        // Get user photos
        $requestUserPhotos = $fb->request('GET', '/me/photos?fields=id,source,name&amp;limit=3');
        // Get uder posts
        //$requestUserPosts = $fb->request('GET', '/me/posts?fields=id,source,name,picture{id,name,source},likes{total_count}');
        $requestUserPosts = $fb->request('GET', '/me/posts?fields=id,source,name,picture{id,name,source},likes{total_count}');
        permalink_url
        $batch = [
            'user-profile'  => $requestUserName,
            'user-photos'   => $requestUserPhotos,
            'user-posts'    => $requestUserPosts
            ];
        echo '<h1>Make a batch request</h1>' . "\n\n";
        try {
          $responses = $fb->sendBatchRequest($batch);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }
        foreach ($responses as $key => $response)
        {
            if ($response->isError())
            {
              $e = $response->getThrownException();
              echo '<p>Error! Facebook SDK Said: ' . $e->getMessage() . "\n\n"
                                    .'<p>Graph Said: ' . "\n\n"
                                    .json_encode($e->getResponse());
            } else
            {
              echo "<p>(" . $key . ") HTTP status code: " . $response->getHttpStatusCode() . "<br />\n";
              echo "Response: " . $response->getBody() . "</p>\n\n";
              echo "<hr />\n\n";
            }
        }
    // */

          // get the last post from FaceBook
          try
          {
              $response = $fb->get('/'.$this->pageId.'?fields=posts.limit(1)');
          }
          catch(Facebook\Exceptions\FacebookResponseException $e)
          {
              // When Graph returns an error
              $result = [
                'status' => 'error',
                'message'   => 'Graph returned an error: ' . $e->getMessage()
                ];
              $response = Response::json($result, 500)->send();
              exit;
          }
          catch(Facebook\Exceptions\FacebookSDKException $e)
          {
              // When validation fails or other local issues
              $result = [
                'status' => 'error',
                'message'   =>  'Facebook SDK returned an error: ' . $e->getMessage()
                ];
              $response = Response::json($result, 500)->send();
              exit;
          }

        try
        {
            //echo"<pre>";var_dump($response->getGraphObject()->asArray()['posts']);echo"</pre>";exit;
            $posts = $response->getGraphObject()->asArray()['posts'];

            $result = [];
            $html = '<article>'
                    .'<div class="card">';
            foreach ($posts as $post){

              if (isset($post['id']))
              {
                  $html .= '<div class="card-body">'
                            .$post['message']
                            .'</div>';
                  // $post['id'] is composed by clientId_objectId
                  // get the objectId
                  $objectId = substr($post['id'],(strpos($post['id'],'_')+1));

                  // request the picture
                  try
                  {
                    $pictureObj = $fb->get('/'.$objectId.'/picture');
                    $picture = $pictureObj->getHeaders();
                    //echo"<pre>";var_dump($picture);echo"</pre>";exit;

                    $post['image_link'] = array_key_exists('Location', $picture) ? $picture['Location'] : '';
                    $html .= '<div class="card-footer">'
                              .'<img src="'. $post['image_link']  .'" class="img-responsive centered" title="posted on facebook"/>'
                            .'</div>';
                  } catch(Facebook\Exceptions\FacebookResponseException $e)
                  {
                    // When Graph returns an error
                    $post['image_link'] = '';
                  } catch(Facebook\Exceptions\FacebookSDKException $e)
                  {
                    // When validation fails or other local issues
                    $post['image_link'] = '';
                  }

                  array_push($result, $post);
                  $html .= '<div class="card-footer centered"> Share it on:'
                            .'<div class="btn-group btn-group-block">'
                            .'<a href="'.$shareFacebook.'" target="_blank" class="btn btn-link share facebook"><i class="fbr-icon-small fbr-icon-facebook"></i> Facebook</a>'                            
                            .'<a href="'.$shareGoogle.'" target="_blank" class="btn btn-link share google"><i class="fbr-icon-small fbr-icon-google"></i> Google+</a>'
                            .'<a href="'.$shareLinkedIn.'" target="_blank" class="btn btn-link share linkedin"><i class="fbr-icon-small fbr-icon-linkedin"></i> LinkedIn</a>'
                            .'<a href="'.$shareTwitter.'" target="_blank" class="btn btn-link share twitter"><i class="fbr-icon-small fbr-icon-twitter"></i> Twitter</a>'
                          .'</div>'
                        .'</div>'
                     .'</div>'
                  .'</article>';
            }
          }
          
          echo $html; exit;
          //return Response::make($html, '200')->header('Content-Type', 'text/xml')->send();
        }
        catch (FacebookRequestException $e)
        {
          $result = [
              'status' => 'error',
              'message'   =>  'could not authorize request. please check your facebook AppID, page id and secret!'
          ];
          $response = Response::json($result, 500)->send();
          exit;
        }

      $result = [
          'status' => 'error',
          'message'   =>  'could not load feed'
      ];
      $response = Response::json($result, 500)->send();
      exit;
    }


}
