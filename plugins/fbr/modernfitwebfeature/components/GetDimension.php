<?php namespace Fbr\ModernFitwebFeature\Components;
use Lang;
//use Auth;
//use Event;
//use Flash;
//use Request;
//use Redirect;
//use Cms\Classes\Page;
use Cms\Classes\ComponentBase;


use Session; // FBR

class GetDimension extends ComponentBase
{
  /* FBR responsive image pattern
  @size-xs: 480px;
  @size-sm: 600px;
  @size-md: 840px;
  @size-lg: 960px;
  @size-xl: 1280px;
  @size-2x: 1440px;*/
    public $breakpoints = [
        360  => 320,
        480  => 440,
        600  => 560,
        840  => 764,
        960  => 900,
        1280 => 1024,
        1440 => 1280,
        1920 => 1600
    ];
    public function componentDetails()
    {
        return [
            'name'        => 'GetDimension',
            'description' => 'Screen Size Component'
        ];
    }

    public function defineProperties()
    {
        return [
            'width' => [
                'title'             => 'Width',
                'default'           => '360',
                'description'       => 'Width in pixels, to be set it as integer.',
                'type'              => 'string',
            ],
            'height' => [
                'title'             => 'Height',
                'default'           => '360',
                'description'       => 'Height in pixels, to be set it as integer.',
                'type'              => 'string',
            ],
            'thumbQuality' => [
                'title'             => 'Image Thumb Quality',
                'default'           => '75',
                'description'       => 'Compress image factor when generating Thumbnails, maximum 100.',
                'type'              => 'string',
            ],
        ];
    }

    public function getSize()
    {
      $screen_width     = 0; // value from Session / default
      $screen_height    = 0;
      $screen_width_ck  = 0; // vlaue from Cookie
      $screen_height_ck = 0;

      $returnArray = array(
            'screen_width'    => $this->property('width'),
            'screen_height'   => $this->property('height'),
            'screen_width_ck' => 0,
            'screen_height_ck'=> 0
          );
///var_dump($returnArray);
      if(array_key_exists('screen_width',$_COOKIE) and !empty($_COOKIE['screen_width'])) {
        ///var_dump(Session::all());
          $screen_width_ck   = $_COOKIE['screen_width'];
          if(Session::has('screen_width')) {
            $screen_width = Session::get('screen_width');
            if($screen_width_ck != $screen_width) {
              $screen_width = $screen_width_ck;
              Session::put('screen_width', $screen_width);
            }
          } else {
            $screen_width = $screen_width_ck;
            Session::put('screen_width', $screen_width);
          }
      } else {
        $screen_width = Session::has('screen_width') ? Session::get('screen_width') : $this->property('width');
      }

      if(array_key_exists('screen_height',$_COOKIE) and !empty($_COOKIE['screen_height'])) {
        ///var_dump(Session::all());
          $screen_height_ck   = $_COOKIE['screen_height'];
          if(Session::has('screen_height')) {
            $screen_height = Session::get('screen_height');
            if($screen_height_ck != $screen_height) {
              $screen_height = $screen_height_ck;
              Session::put('screen_height', $screen_height);
            }
          } else {
            $screen_height = $screen_height_ck;
            Session::put('screen_height', $screen_height);
          }
      } else {
          $screen_height = Session::has('screen_height') ? Session::get('screen_height') : $this->property('height');
      }
      ///var_dump(Session::all());
      $returnArray['screen_width']   = (int)$screen_width;
      $returnArray['screen_height']  = (int)$screen_height;
      $returnArray['session_token']  = Session::get('_token');

      $returnArray['screen_width_ck']     = (int)$screen_width_ck;
      $returnArray['screen_height_ck']    = (int)$screen_height_ck;
      // determine device orientation
      $returnArray['screen_orientation']  = ($returnArray['screen_width'] < $returnArray['screen_height']) ? 'portrait' : 'landscape';
      $thumbQuality = (int)$this->property('thumbQuality');
      $returnArray['thumb_quality']  = $thumbQuality > 0 ? $thumbQuality : 75;
      // get values ['closest_width'] / ['closest_height']
      $returnArray = array_merge($returnArray,
                                 $this->getClosestValToScreenSize(
                                                                  $screen_width,
                                                                  $screen_height
                                ));

      return $returnArray;

    }

    /**
    * return an array with closest_width/height values
    */
    public function  getClosestValToScreenSize($aScreenWidth, $aScreenHeight)
    {
        $ret['closest_width']   = $this->computeClosestValToScreenSize($aScreenWidth);
        $ret['closest_height']  = $this->computeClosestValToScreenSize($aScreenHeight);

        return $ret;
    }

    /**
    * compute the closest  value of the screen, reported to predefined breakpoins
    * returns: integer
    */
    private function  computeClosestValToScreenSize($findItValue)
    {

        $safeMarginWidth = 0; // value in pixels, to keep a horizontal distance from edges
        $arrClosestToScreenSize = $this->breakpoints;
        $fixedArr   = array_keys($arrClosestToScreenSize);
        $valuesArr  = array_values($arrClosestToScreenSize);

        //if($aScreenOrientation == 'portrait') {
          // closest value reported on height
        //  $findItValue =  $aScreenHeight;
          // unomment below only when  $arrClosestToScreenSize = Config::get('imagePopupGalleryWidthToHeight', false);
          // $fixedArr   = array_values($arrClosestToScreenSize);
          // $valuesArr  = array_keys($arrClosestToScreenSize);
        //}
        asort($fixedArr);
        asort($valuesArr);

    ///echo "<h1>getClosestToScreenSize</h1>";var_dump($fixedArr);var_dump($valuesArr);var_dump($find);

         foreach($fixedArr as $key => $val) {
           if($val <= $findItValue){
               $large[$key] = $val;
           } else {
               $small[$key] = $val;
           }
         }

       if(isset($large)) {
          $near1 = max($large);
       }
       if(isset($small)) {
          $near2 = min($small);
       }
       if(!isset($near1)) {
         reset($fixedArr);
         $near1 = current($fixedArr);
         $key = array_search($near1, $fixedArr);
         return $valuesArr[$key];
       }
       if(!isset($near2)) {
         $near2 = end($fixedArr);
         $key = array_search($near2, $fixedArr);
         return $valuesArr[$key];
       }

        if($findItValue >= ($near1 + $near2) / 2) {
          $key = array_search($near2, $fixedArr);
          return $valuesArr[$key];
        } else {
          $key = array_search($near1, $fixedArr);
          return $valuesArr[$key];
        }

    }
}
