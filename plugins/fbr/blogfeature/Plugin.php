<?php namespace Fbr\BlogFeature;
// ALTER TABLE `system_files` ADD `image_width` INT(6) UNSIGNED NULL DEFAULT '0' AFTER `updated_at`, ADD `image_height` INT(6) UNSIGNED NULL DEFAULT '0' AFTER `image_width`;
use Backend;
use Event;

use System\Classes\PluginManager;
use System\Classes\PluginBase;

use Db;
use Url;
use App;
use Str;
use Html;
use Lang;
use Model;
use Markdown;
use BackendAuth;
use ValidationException;
use RainLab\Blog\Classes\TagProcessor;
use Backend\Models\User;
use Carbon\Carbon;
use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;

use RainLab\Blog\Models\Post;
use RainLab\Blog\Models\Category;
use Fbr\BlogFeature\Models\BlogFeature;

use Config; /* FBR */

class Plugin extends PluginBase
{
	public $require = ['RainLab.Blog'];
//*
	public function registerComponents()
	{
		return [
				'Fbr\BlogFeature\Components\BlogFeatures' => 'BlogFeatures'
		];
	}

// */
    public function boot()
    {/*
    	Post::extend(function($model){
    		$model->hasOne['feature'] = ['\Fbr\BlogFeature\Models\BlogFeature', 'foreignKey' => 'feature_id'];
    	});
    	*/
    	parent::boot();
    	Event::listen('backend.form.extendFields', function($widget)
    	{

	    	if(PluginManager::instance()->hasPlugin('RainLab.Blog') && $widget->model instanceof \RainLab\Blog\Models\Post)
	    	{
	    		$widget->addFields([
	    				'feature_icon'=> [
								'comment' => 'fbr.blogfeature::lang.feature_icon.comment',
								'label' => 'fbr.blogfeature::lang.feature_icon.label',
								'options' => BlogFeature::getBlogFeatureIconOptions(),
								'span' => 'left',
								'type' => 'dropdown',
	    						'size' => 'small',
	    						'tab' => 'Homepage'
	    						],
	    				'feature_homepage'=> [
	    						'comment' => 'fbr.blogfeature::lang.feature_homepage.comment',
	    						'label' => 'fbr.blogfeature::lang.feature_homepage.label',
	    						'options' => BlogFeature::getBlogFeatureHomepageOptions(),
	    						'span' => 'right',
	    						'type' => 'dropdown',
	    						'tab' => 'Homepage'
	    				]
	    		],
	    		'secondary');

					$widget->addCss('/themes/'.Config::get('cms.activeTheme', false).'/assets/css/icons.data.svg.css'); /* FBR */
	    	}
    	});

    	Event::listen('backend.filter.extendScopes', function ($widget) {
    		// Only for the Posts controller
    		if (( ! $widget->getController() instanceof \Rainlab\Blog\Controllers\Posts)) {
    			return;
    		}

    		$widget->addScopes([
    				'FeatureHomepage' => [
    						'label'      => 'Feature Homepage',
    						'type'       => 'checkbox',
    						'scope'      => 'filterFeatureHomepage'
    				],
    		]);
    	});

    	\Rainlab\Blog\Models\Post::extend(function ($model) {
    		//$model::$allowedSortingOptions ['feature_homepage asc'] = 'Feature homepage'; // Fbr/BlogFeature
    			// Extend the constructor of the model
    			$model->addDynamicMethod('scopeFilterFeatureHomepage', function ($query) {

    				return
						$query->where('feature_homepage', '>', 0)
    							->orderBy('feature_homepage','asc');
    			});

    	});
    	/*
    	// upgrade already through backend / Settings /Plugins manager
    	\Rainlab\Blog\Models\Post::extend(function ($model) {
    			// Extend the constructor of the model
    		$model->addDynamicMethod('scopeListFrontEnd', function ($query, $options)
    		{
    			// Default options

    			extract(array_merge([
    					'page'       => 1,
    					'perPage'    => 30,
    					'sort'       => 'created_at',
    					'categories' => null,
    					'category'   => null,
    					'search'     => '',
    					'published'  => true,
    					'exceptPost' => null,
    			], $options));

    			$searchableFields = ['title', 'slug', 'excerpt', 'content'];

    			if ($published) {
    				$query->isPublished();
    			}

    			// Ignore a post

    			if ($exceptPost) {
    				if (is_numeric($exceptPost)) {
    					$query->where('id', '<>', $exceptPost);
    				}
    				else {
    					$query->where('slug', '<>', $exceptPost);
    				}
    			}

    			// Search

    			$search = trim($search);
    			if (strlen($search)) {
    				$query->searchWhere($search, $searchableFields);
    			}
    			// Categories

    			if ($categories !== null) {
    				if (!is_array($categories)) $categories = [$categories];
    				$query->whereHas('categories', function($q) use ($categories) {
    					$q->whereIn('id', $categories);
    				});
    			}

    			// Category, including children

    			if ($category !== null) {
    				$category = Category::find($category);

    				$categories = $category->getAllChildrenAndSelf()->lists('id');

    				if ($category->slug == 'homepage') {
    					$query->where('feature_homepage', '>', 0)->orderBy('feature_homepage','asc')
    					->whereHas('categories', function($q) use ($categories) {
    						$q->whereIn('id', $categories);
    					});
    				}else {
	    				$query->whereHas('categories', function($q) use ($categories) {
	    					$q->whereIn('id', $categories);
	    				});
    				}
    			}

    			// Sorting

    			if (!is_array($sort)) {
    				$sort = [$sort];
    			}

    			foreach ($sort as $_sort) {

    				if (in_array($_sort, array_keys(Post::$allowedSortingOptions))) {
    					$parts = explode(' ', $_sort);
    					if (count($parts) < 2) {
    						array_push($parts, 'desc');
    					}
    					list($sortField, $sortDirection) = $parts;
    					if ($sortField == 'random') {
    						$sortField = Db::raw('RAND()');
    					}
    					$query->orderBy($sortField, $sortDirection);
    				}
    			}


    			return $query->paginate($perPage, $page);
    			});


    		});
    	// */

    }



    /*
    public function boot()
    {
    	parent::boot();

    	Post::extend(function($model){
    		$model->hasOne['feature'] = ['\Fbr\BlogFeature\Models\BlogFeature'];
    	});

    	Event::listen('backend.form.extendFields', function($widget) {
    				if (!$widget->getController() instanceof \RainLab\Blog\Controllers\Posts) return;

    				//if ($widget->getContext() != 'update') return;

    				if (!BlogFeature::getFromPost($widget->model)) return;

    				$widget->addFields([
    						'feature[icon_data]' => [
    								'comment' => 'fbr.blogfeature::lang.fields.icon_data.comment',
    								'label' => 'fbr.blogfeature::lang.fields.icon_data.label',
    								'options' => BlogFeature::getBlogFeatureIconDataOptions(),
    								'span' => 'left',
    								'type' => 'dropdown'
    						]
    				], 'primary');
    			});

    }
    */


}
