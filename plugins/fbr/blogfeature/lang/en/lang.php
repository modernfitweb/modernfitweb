<?php
return [
		'plugin' => [
				'name' => 'Blog Features',
				'description' => 'PREREQUISITES:
- add a icon to the blog from the theme"s icons.data.svg.css
- add a homepage area for each published post
- add a language pack for romanian language'
		],

		'feature_homepage' => [
				'comment' => 'Add this post to the homepage, please select the desired area.',
				'label' => 'Homepage area'
		],
		'homepage_options' => [
				'slideshow_heading_excerpt_btn' 		=> 'Zona 1: Carousel image(Title,Desc,), Heading, Excerpt, Btn.',
				'panel_text_image' 									=> 'Zona 2: Panel colored, alternate left/right Text with Image',
				'icon_title_excerpt_btn' 						=> 'Zona 3: Icon, Title, Excerpt, Btn.',
				'alternate_image_title_excerpt_btn' => 'Zona 4: Alternate text left/right, Title, Image, Excerpt, Btn.',
				'figure_title_image' 								=> 'Zona 5: Post`s Gallery  with hover effect',
				'panel_without_image' 							=> 'Zona 6: Text panel Without Image',
				'switcher_words'										=> 'Zona 7: Animated pictures, Switcher gen. from images titles, Btn.',
		]

];
