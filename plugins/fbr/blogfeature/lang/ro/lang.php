<?php
return [
		'plugin' => [
				'name' => 'Blog Features',
				'description' => 'PREREQUISITES:
- add a icon to the blog from the theme"s icons.data.svg.css
- add a homepage area for each published post
- add a language pack for romanian language'
		],

		'feature_homepage' => [
				'comment' => 'Add this post to the homepage, please select the desired area.',
				'label' => 'Homepage area'
		],
		'homepage_options' => [
			'slideshow_heading_excerpt_btn' 		=> 'Zona 1: Carousel poze (Titlu,Descr.), Titlul si Subtitlul articolului .',
			'panel_text_image' 									=> 'Zona 2: Panou colorat cu Text si Imagine alternativ pe st./dr.',
			'icon_title_excerpt_btn' 						=> 'Zona 3: Icana, Titlu, Introducere, Btn.',
			'alternate_image_title_excerpt_btn' => 'Zona 4: Titlu, Imagine, Introducere, Btn. alternativ pe st./dr.',
			'figure_title_image' 								=> 'Zona 5: Galerie de poze ale articolului',
			'panel_without_image' 							=> 'Zona 6: Panou de text fara Imagine',
			'switcher_words'										=> 'Zona 7: Poze animate si Btn.de comutare cu text din titlul pozei',
		]

];
