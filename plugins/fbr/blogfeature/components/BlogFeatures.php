<?php namespace Fbr\BlogFeature\Components;

use Cms\Classes\ComponentBase;
use Event;
use Fbr\BlogFeature\Models\BlogFeature;
class BlogFeatures extends ComponentBase
{

    public $page;
    public $feature_icon;
    public $feature_homepage;
    public $blog_feature_icon_arr = array();

    public function componentDetails()
    {
        return [
            'name'        => 'Blog Feature',
            'description' => 'Blog Feature description'
        ];
    }

    public function defineProperties()
    {
        return [
            "post" => [
                "title" => "data",
                "default" => "post"
            ]
        ];
    }
    public function onInit() {
    }

}
