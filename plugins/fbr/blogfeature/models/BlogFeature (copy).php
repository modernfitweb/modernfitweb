<?php namespace Fbr\BlogFeature\Models;

use Model;
use Lang;
use System\Classes\PluginManager;

use RainLab\Blog\Models\Post;
/**
 * Model
 */
class BlogFeature extends Model
{
	public $require = ['RainLab.Blog'];
	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'fbr_blogfeatures';
	public $timestamps = false;

	/**
	 * The attributes on which the post list can be ordered
	 * @var array
	 */

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = [
	];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = [
			'feature_icon', 'feature_homepage'
	];
    /**
     * @var string The database table used by the model.
     */
	protected $jsonable = [
	];

    public $belongsTo = [

    ];



    public static function boot()
    {
    	// Call default functionality (required)
    	//parent::boot();

    }
    public static function getBlogFeatureSwitcherWordsOptions($letter = null) // getUserProfileStatusOptions()
    {	// http://www.wordle.net/create
    	// http://www.wordle.net/create
    $words['a'] = 'acceptare, acord, acţiune, activ, acurateţe, adaptabilitate, adăugare, admirabil, adorabil, afecţiune, afirmare, agilitate, agreabil, ajutor, alegere, altruism, ambiţie, amuzament, anticipare, apartenenţă, apreciere, aprobare, armonie, asertivitate, atenţie, autenticitate, autocontrol, autonomie, avantaj, aventură ';
    $words['b'] = 'beneficiu, bine, binecuvântare, binefacere, binevoitoare, blândeţe, bravo, bucurie, bunătate, bunăvoinţă';
    $words['c'] = 'calm, capabil, caritate, certitudine, cinste, claritate, colaborare, compasiune, competenţă, comunicare, comunitate, concentrare, conexiune, confort, consecvenţă, consolidare, conştientizare, conştiinciozitate, continuitate, contribuţie, control, convingere, cooperare, corectitudine, creaţie, creativitate, credinţă, curaj, curăţenie, curiozitate ';
    $words['d'] = 'datorie, decenţă, delicat, delicios, demnitate, desăvârşire, descoperire, detaşare, determinare, devotament, dinamică, direcţie, disciplină, discreţie, distracţie, diversitate, divinitate, dobândire, dorinţă, dorit, dragoste, dreptate, duh, duhovnicesc';
    $words['e'] = 'echilibru, educaţie, eficienţă, egalitate, eleganţă, emoţie, empatie, emulare, energie, entuziasm, etern, euforie, evlavie, exaltare, excelenţă, experienţă, expertiză, explorare, expresivitate, exprimare, extraordinar, exuberanţă ';
    $words['f'] = 'fabulos, faimă, familie, fantastic, fericire, fiabilitate, fidelitate, flexibilitate, floare, focus, forţă, frumos, frumuseţe';
    $words['g'] = 'generozitate, genial, graţie';
    $words['h'] = 'har, hărnicie, hotărâre';
    $words['i'] = 'idee, iertare, imaginaţie, îmbărbătare, îmbogăţire, îmbunătăţire, împăcare, imparţialitate, împreună, împrospătare, împuternicire, inălţare, încântare, încredere, incredibil, încurajare, independenţa, individualism, îndrăzneală, înfăţişare, infinit, influenţă, informativ, înfrumuseţare, îngăduinţă, ingeniozitate, inimă, inovare, inspiraţie, insufleţire, întâmpinare, întărire, integritate, înţelegere, înţelepciune, înţeles, inteligenţă, intensitate, intenţie, interes, interesant, interior, intimitate, întrajutorare, întrupare, intuiţie, inventivitate, investiţie, înviere, irezistibil, iubire';
    $words['j'] = 'jovialitate, justiţie, just';
    $words['l'] = 'lăudabil, libertate, lider, limpede, logică, loialitate, longevitate, lumină';
    $words['m'] = 'magie, magnific, maiestuos, mândrie, mângâiere, mântuire, mărinimie, mărturie, maturitate, merit, milă, milostenie, minunat, minune, miracol, miraculoasă, miraculos, miruit, moderaţie, modestie, moralitate, motivare, mulţumesc, mulţumire ';
    $words['n'] = 'nădejde, neasemănat, neînfricat, nelimitat, nemărginit, nobil, nonviolenţă';
    $words['o'] = 'omagiu, omenie, onoare, oportunitate, optimism, ordine, organizare, orientare, originalitate, ospitalitate';
    $words['p'] = 'pace, paradis, pasiune, perfecţiune, perseverenţă, plăcere, plăcut, plenitudine, plinătate, plus, pocăinţă, politeţe, potenţial, pozitivitate, practic, preafericit, preamărire, precizie, pregătire, prezenţă, prietenie, proactivitate, progres, prolific, prosperitate, prudenţă, punctualitate, pur, puritate, putere, putinţă';
    $words['r'] = 'răbdare, radiant, raţionalitate, realitate, realizare, reasigurare, recomandare, recunoştinţă, relaţie, relaxare, religie, respect, responsabilitate, revigorant, rezistenţă, ridicare, rugăciune ';
    $words['s'] = 'sacru, salvare, satisfacere, securitate, seninătate, sensibilitate, sentiment, serenitate, servi, siguranţă, simpatie, simplificare, simplitate, sinceritate, sine, sistematizare, slavă, speranţa, spirit, splendid, sporire, spornicie, stabilitate, ştinţa, strălucire, suflet, suprem, susţine, serenitate ';
    $words['t'] = 'tact, tandreţe, tare, tenacitate, toleranţă, tradiţie, trezire, triumfător';
    $words['u'] = 'uimitor, uman, umor, unic, unire, unitate, uşor, uşurare, uşurinţă, util, utilizare';
    $words['v'] = 'validare, valoare, varietate, verificare, verosimil, veselie, veşnicie, viabil, vibrant, victorie, vigilenţă, virtute, vitalitate, viu, vivace, voinţă, voios, voioşie, vulnerabilitate';
   if($letter == null)
   	 return $words;
   else
   	return $words[$letter];
    }

    public static function shuffleSwitcherWordsOptions()
    {
    	$words_union = array();
    	$words_shuffle = array();
    	$words_alphabet = self::getBlogFeatureSwitcherWordsOptions();
    	foreach ($words_alphabet as $k => $val) {
    		$tmp_arr = explode(',', $val);
    		foreach ($tmp_arr as $kk => $vv) {
    			$words_union[] = trim($vv);
    		}
    	}
    	$words_shuffle = $words_union;
    	shuffle($words_shuffle);
    	return array('all' => implode(',', $words_union), 'shuffle' => implode(',', $words_shuffle));
    }
    public static function chunkSwitcherWordsOptions($str_csv, $chunk_size = 40)
    {
    	$kon = 1;
    	$tmp_arr = explode(',', $str_csv);
    	$ret_arr = array();
    	$tmp = array();
    	foreach ($tmp_arr as $kk => $vv) {
    		$tmp[] = trim($vv);
    		if ($kon % $chunk_size == 0) {
    			$ret_arr[] = implode(',', $tmp);
    			//var_dump(implode(',', $tmp));

    			$tmp = array();
    		}
    		$kon++;
    	}
    	$ret_arr[] = implode(',', $tmp);
    	return $ret_arr;
    }

    public static function getRandomSwitcherWordsOptions()
    {
    	$ret = array(
1 =>'respect,spirit,plus,idee,politeţe,modestie,delicat,emulare,competenţă,aprobare,entuziasm,floare,uman,uimitor,apreciere,justiţie,interior,agreabil,ambiţie,progres,irezistibil,continuitate,altruism,binecuvântare,distracţie,incredibil,putinţă,omagiu,calm,putere,colaborare,îmbărbătare,merit,prosperitate,decenţă,maiestuos,radiant,întrajutorare,admirabil,securitate',
2 =>'sine,inventivitate,individualism,pocăinţă,afecţiune,splendid,blândeţe,echilibru,flexibilitate,autenticitate,excelenţă,validare,miruit,dorinţă,neasemănat,conştientizare,slavă,exprimare,uşurinţă,vitalitate,plăcere,împreună,neînfricat,extraordinar,lăudabil,salvare,moralitate,bucurie,mângâiere,înfăţişare,întâmpinare,perseverenţă,moderaţie,nonviolenţă,intimitate,just,milostenie,strălucire,potenţial,interes',
3 =>'etern,ajutor,uşor,graţie,omenie,bunăvoinţă,punctualitate,convingere,pozitivitate,corectitudine,reasigurare,serenitate,victorie,creativitate,sensibilitate,duh,anticipare,emoţie,paradis,dragoste,tenacitate,mulţumesc,genial,forţă,direcţie,curăţenie,expertiză,mărturie,descoperire,mărinimie,delicios,vibrant,confort,împuternicire,pur,sinceritate,perfecţiune,viu,vigilenţă,acurateţe',
4 =>'asertivitate,unitate,încântare,detaşare,împăcare,suflet,pasiune,valoare,binefacere,investiţie,conexiune,independenţa,speranţa,exuberanţă,miraculoasă,realizare,miracol,control,dreptate,infinit,satisfacere,voios,ştinţa,activ,fericire,demnitate,adorabil,minunat,nemărginit,ingeniozitate,milă,proactivitate,desăvârşire,avantaj,mândrie,înţelegere,precizie,fidelitate,îmbunătăţire,eleganţă',
5 =>'pace,dobândire,concentrare,iubire,longevitate,inălţare,tradiţie,atenţie,expresivitate,virtute,magnific,motivare,umor,vulnerabilitate,prezenţă,adăugare,suprem,servi,simpatie,dinamică,datorie,veşnicie,intensitate,imparţialitate,focus,vivace,influenţă,integritate,ordine,serenitate,conştiinciozitate,practic,fiabilitate,sistematizare,magie,încurajare,certitudine,aventură,euforie,recunoştinţă',
6 =>'educaţie,organizare,înfrumuseţare,autonomie,binevoitoare,inovare,curaj,agilitate,insufleţire,intenţie,varietate,stabilitate,prietenie,armonie,mântuire,înţeles,lider,ridicare,frumuseţe,apartenenţă,viabil,limpede,preamărire,logică,realitate,acord,unire,curiozitate,îndrăzneală,hotărâre,beneficiu,susţine,preafericit,seninătate,libertate,autocontrol,acceptare,recomandare,har,răbdare',
7 =>'puritate,simplitate,inspiraţie,energie,raţionalitate,experienţă,comunicare,lumină,loialitate,întrupare,duhovnicesc,rezistenţă,întărire,intuiţie,îngăduinţă,oportunitate,divinitate,înţelepciune,cinste,diversitate,sacru,veselie,îmbogăţire,interesant,prudenţă,utilizare,încredere,voinţă,util,sentiment,iertare,consecvenţă,compasiune,revigorant,contribuţie,familie,înviere,hărnicie,faimă,prolific',
8 =>'amuzament,trezire,cooperare,miraculos,exaltare,unic,minune,sporire,plenitudine,voioşie,uşurare,capabil,caritate,creaţie,bine,comunitate,frumos,fantastic,eficienţă,maturitate,siguranţă,verificare,simplificare,toleranţă,inteligenţă,spornicie,acţiune,nelimitat,generozitate,orientare,tare,egalitate,dorit,devotament,verosimil,fabulos,bunătate,nădejde,credinţă,nobil',
9 =>'rugăciune,jovialitate,afirmare,tandreţe,pregătire,onoare,relaţie,plăcut,imaginaţie,alegere,optimism,determinare,ospitalitate,disciplină,plinătate,religie,evlavie,discreţie,adaptabilitate,bravo,inimă,informativ,empatie,tact,claritate,explorare,consolidare,împrospătare,responsabilitate,mulţumire,originalitate,triumfător,relaxare'

    	);
    }
    public static function displaySwitcherWordsOptions($chunk_size = 40)
    {
    	$tmp = self::shuffleSwitcherWordsOptions();
    	echo "all positive words:<br>";
    	echo "<pre>";
    	echo $tmp['all'];
    	echo "</pre><hr>";
    	echo "Group of $chunk_size of randomised positive words:<br>";
    	$ass =  self::chunkSwitcherWordsOptions($tmp['shuffle'], $chunk_size);
    	foreach ($ass as $kk => $vv) {
    		echo "<pre>";
    		echo $vv;
    		echo "</pre><br><br>";
    	}

    }
    /**
     * get custom icons
     * @return array[]
     */
    public static function getBlogFeatureIconOptions() // getUserProfileStatusOptions()
    {
    		return array(
    			0 => '',
    			1 => '<i class="fbr-icon-medium uk-icon-accessibility"></i>',
    			2 => '<i class="fbr-icon-medium uk-icon-aid-kit"></i>',
    			3 => '<i class="fbr-icon-medium uk-icon-airplane"></i>',
    			4 => '<i class="fbr-icon-medium uk-icon-album"></i>',
    			5 => '<i class="fbr-icon-medium uk-icon-attachment"></i>',
    			6 => '<i class="fbr-icon-medium uk-icon-ban"></i>',
    			7 => '<i class="fbr-icon-medium uk-icon-bell"></i>',
    			8 => '<i class="fbr-icon-medium uk-icon-bike"></i>',
    			9 => '<i class="fbr-icon-medium uk-icon-binoculars"></i>',
    			10 => '<i class="fbr-icon-medium uk-icon-bolt"></i>',
    			11 => '<i class="fbr-icon-medium uk-icon-book"></i>',
    			12 => '<i class="fbr-icon-medium uk-icon-bookmark"></i>',
    			13 => '<i class="fbr-icon-medium uk-icon-books"></i>',
    			14 => '<i class="fbr-icon-medium uk-icon-briefcase"></i>',
    			15 => '<i class="fbr-icon-medium uk-icon-bullhorn"></i>',
    			16 => '<i class="fbr-icon-medium uk-icon-calendar"></i>',
    			17 => '<i class="fbr-icon-medium uk-icon-camera"></i>',
    			18 => '<i class="fbr-icon-medium uk-icon-cart"></i>',
    			19 => '<i class="fbr-icon-medium uk-icon-check"></i>',
    			20 => '<i class="fbr-icon-medium uk-icon-checkmark"></i>',
    			21 => '<i class="fbr-icon-medium uk-icon-checkmark2"></i>',
    			22 => '<i class="fbr-icon-medium uk-icon-chevron-down""></i>',
    			23 => '<i class="fbr-icon-medium uk-icon-chevron-left"></i>',
    			24 => '<i class="fbr-icon-medium uk-icon-chevron-right"></i>',
    			25 => '<i class="fbr-icon-medium uk-icon-chevron-up"></i>',
    			26 => '<i class="fbr-icon-medium uk-icon-clock"></i>',
    			27 => '<i class="fbr-icon-medium uk-icon-close-icon"></i>',
    			28 => '<i class="fbr-icon-medium uk-icon-close-large"></i>',
    			29 => '<i class="fbr-icon-medium uk-icon-close"></i>',
    			30 => '<i class="fbr-icon-medium uk-icon-code"></i>',
    			31 => '<i class="fbr-icon-medium uk-icon-commenting"></i>',
    			32 => '<i class="fbr-icon-medium uk-icon-comments"></i>',
    			33 => '<i class="fbr-icon-medium uk-icon-contrast"></i>',
    			34 => '<i class="fbr-icon-medium uk-icon-contrast2"></i>',
    			35 => '<i class="fbr-icon-medium uk-icon-copy"></i>',
    			36 => '<i class="fbr-icon-medium uk-icon-credit-card"></i>',
    			37 => '<i class="fbr-icon-medium uk-icon-desktop"></i>',
    			38 => '<i class="fbr-icon-medium uk-icon-eye"></i>',
    			39 => '<i class="fbr-icon-medium uk-icon-facebook"></i>',
    			40 => '<i class="fbr-icon-medium uk-icon-edit"></i>',
    			41 => '<i class="fbr-icon-medium uk-icon-fire"></i>',
    			42 => '<i class="fbr-icon-medium uk-icon-flag-de"></i>',
    			43 => '<i class="fbr-icon-medium uk-icon-flag-en"></i>',
    			44 => '<i class="fbr-icon-medium uk-icon-flag-fr"></i>',
    			45 => '<i class="fbr-icon-medium uk-icon-flag-ro"></i>',
    			46 => '<i class="fbr-icon-medium uk-icon-flag"></i>',
    			47 => '<i class="fbr-icon-medium uk-icon-flickr"></i>',
    			48 => '<i class="fbr-icon-medium uk-icon-folder"></i>',
    			49 => '<i class="fbr-icon-medium uk-icon-form-checkbox-indeterminate"></i>',
    			50 => '<i class="fbr-icon-medium uk-icon-form-checkbox"></i>',
    			51 => '<i class="fbr-icon-medium uk-icon-form-radio"></i>',
    			52 => '<i class="fbr-icon-medium uk-icon-form-select"></i>',
    			53 => '<i class="fbr-icon-medium uk-icon-forward"></i>',
    			54 => '<i class="fbr-icon-medium uk-icon-foursquare"></i>',
    			55 => '<i class="fbr-icon-medium uk-icon-future"></i>',
    			56 => '<i class="fbr-icon-medium uk-icon-gift"></i>',
    			57 => '<i class="fbr-icon-medium uk-icon-glass"></i>',
    			58 => '<i class="fbr-icon-medium uk-icon-glass2"></i>',
    			59 => '<i class="fbr-icon-medium uk-icon-google-plus"></i>',
				60 => '<i class="fbr-icon-medium uk-icon-google"></i>',
				61 => '<i class="fbr-icon-medium uk-icon-grid"></i>',
				62 => '<i class="fbr-icon-medium uk-icon-hammer"></i>',
				63 => '<i class="fbr-icon-medium uk-icon-hammer2"></i>',
				64 => '<i class="fbr-icon-medium uk-icon-hashtag"></i>',
				65 => '<i class="fbr-icon-medium uk-icon-heart-broken"></i>',
				66 => '<i class="fbr-icon-medium uk-icon-heart-full"></i>',
				67 => '<i class="fbr-icon-medium uk-icon-heart"></i>',
				68 => '<i class="fbr-icon-medium uk-icon-history"></i>',
				69 => '<i class="fbr-icon-medium uk-icon-home"></i>',
				70 => '<i class="fbr-icon-medium uk-icon-image"></i>',
   				71 => '<i class="fbr-icon-medium uk-icon-info"></i>',
   				72 => '<i class="fbr-icon-medium uk-icon-instagram"></i>',
   				73 => '<i class="fbr-icon-medium uk-icon-italic"></i>',
   				74 => '<i class="fbr-icon-medium uk-icon-lab"></i>',
   				75 => '<i class="fbr-icon-medium uk-icon-laptop"></i>',
   				76 => '<i class="fbr-icon-medium uk-icon-library"></i>',
   				77 => '<i class="fbr-icon-medium uk-icon-link"></i>',
   				78 => '<i class="fbr-icon-medium uk-icon-linkedin"></i>',
   				79 => '<i class="fbr-icon-medium uk-icon-list-bullet"></i>',
   				80 => '<i class="fbr-icon-medium uk-icon-list"></i>',
   				81 => '<i class="fbr-icon-medium uk-icon-location"></i>',
   				82 => '<i class="fbr-icon-medium uk-icon-lock"></i>',
   				83 => '<i class="fbr-icon-medium uk-icon-magnet"></i>',
   				84 => '<i class="fbr-icon-medium uk-icon-mail"></i>',
   				85 => '<i class="fbr-icon-medium uk-icon-man-woman"></i>',
   				86 => '<i class="fbr-icon-medium uk-icon-man"></i>',
   				87 => '<i class="fbr-icon-medium uk-icon-menu"></i>',
   				88 => '<i class="fbr-icon-medium uk-icon-minus"></i>',
   				89 => '<i class="fbr-icon-medium uk-icon-more-vertical"></i>',
   				90 => '<i class="fbr-icon-medium uk-icon-more"></i>',
   				91 => '<i class="fbr-icon-medium uk-icon-move"></i>',
   				92 => '<i class="fbr-icon-medium uk-icon-mug"></i>',
   				93 => '<i class="fbr-icon-medium uk-icon-nav-parent-close"></i>',
   				94 => '<i class="fbr-icon-medium uk-icon-nav-parent-open"></i>',
   				95 => '<i class="fbr-icon-medium uk-icon-navbar-toggle-icon"></i>',
   				96 => '<i class="fbr-icon-medium uk-icon-nut"></i>',
   				97 => '<i class="fbr-icon-medium uk-icon-office"></i>',
   				98 => '<i class="fbr-icon-medium uk-icon-overlay-icon"></i>',
   				99 => '<i class="fbr-icon-medium uk-icon-pagekit"></i>',
   				100 => '<i class="fbr-icon-medium uk-icon-pagination-next"></i>',
   				101 => '<i class="fbr-icon-medium uk-icon-pagination-previous"></i>',
   				102 => '<i class="fbr-icon-medium uk-icon-paint-bucket"></i>',
   				103 => '<i class="fbr-icon-medium uk-icon-pencil"></i>',
   				104 => '<i class="fbr-icon-medium uk-icon-phone-landscape"></i>',
   				105 => '<i class="fbr-icon-medium uk-icon-phone"></i>',
   				106 => '<i class="fbr-icon-medium uk-icon-pinterest"></i>',
   				107 => '<i class="fbr-icon-medium uk-icon-play"></i>',
   				108 => '<i class="fbr-icon-medium uk-icon-plus"></i>',
   				109 => '<i class="fbr-icon-medium uk-icon-point-down"></i>',
   				110 => '<i class="fbr-icon-medium uk-icon-point-left"></i>',
   				111 => '<i class="fbr-icon-medium uk-icon-point-right"></i>',
   				112 => '<i class="fbr-icon-medium uk-icon-point-up"></i>',
   				113 => '<i class="fbr-icon-medium uk-icon-power"></i>',
   				114 => '<i class="fbr-icon-medium uk-icon-pull"></i>',
   				115 => '<i class="fbr-icon-medium uk-icon-push"></i>',
   				116 => '<i class="fbr-icon-medium uk-icon-puzzle"></i>',
   				117 => '<i class="fbr-icon-medium uk-icon-question"></i>',
   				118 => '<i class="fbr-icon-medium uk-icon-quote-left"></i>',
   				119 => '<i class="fbr-icon-medium uk-icon-quote-right"></i>',
   				120 => '<i class="fbr-icon-medium uk-icon-receiver"></i>',
   				121 => '<i class="fbr-icon-medium uk-icon-refresh"></i>',
   				122 => '<i class="fbr-icon-medium uk-icon-reply"></i>',
   				123 => '<i class="fbr-icon-medium uk-icon-rocket"></i>',
   				124 => '<i class="fbr-icon-medium uk-icon-rss"></i>',
   				125 => '<i class="fbr-icon-medium uk-icon-search-default"></i>',
   				126 => '<i class="fbr-icon-medium uk-icon-server"></i>',
   				127 => '<i class="fbr-icon-medium uk-icon-settings"></i>',
   				128 => '<i class="fbr-icon-medium uk-icon-shield"></i>',
   				129 => '<i class="fbr-icon-medium uk-icon-shrink"></i>',
   				130 => '<i class="fbr-icon-medium uk-icon-sign-in"></i>',
   				131 => '<i class="fbr-icon-medium uk-icon-sign-out"></i>',
   				132 => '<i class="fbr-icon-medium uk-icon-skype"></i>',
   				133 => '<i class="fbr-icon-medium uk-icon-social"></i>',
   				134 => '<i class="fbr-icon-medium uk-icon-soundcloud"></i>',
   				135 => '<i class="fbr-icon-medium uk-icon-spinner"></i>',
   				136 => '<i class="fbr-icon-medium uk-icon-spinner2"></i>',
   				137 => '<i class="fbr-icon-medium uk-icon-spinner3"></i>',
   				138 => '<i class="fbr-icon-medium uk-icon-spoon-knife"></i>',
   				139 => '<i class="fbr-icon-medium uk-icon-star-empty"></i>',
   				140 => '<i class="fbr-icon-medium uk-icon-star-star-half"></i>',
   				141 => '<i class="fbr-icon-medium uk-icon-star"></i>',
   				142 => '<i class="fbr-icon-medium uk-icon-stats-dots"></i>',
   				144 => '<i class="fbr-icon-medium uk-icon-sun"></i>',
   				145 => '<i class="fbr-icon-medium uk-icon-table"></i>',
					146 => '<i class="fbr-icon-medium uk-icon-tablet-landscape"></i>',
					147 => '<i class="fbr-icon-medium uk-icon-tablet"></i>',
					148 => '<i class="fbr-icon-medium uk-icon-tag"></i>',
					149 => '<i class="fbr-icon-medium uk-icon-target"></i>',
					150 => '<i class="fbr-icon-medium uk-icon-thumbnails"></i>',
					151 => '<i class="fbr-icon-medium uk-icon-totop"></i>',
					152 => '<i class="fbr-icon-medium uk-icon-trash"></i>',
					153 => '<i class="fbr-icon-medium uk-icon-triangle-down"></i>',
					154 => '<i class="fbr-icon-medium uk-icon-triangle-left"></i>',
					155 => '<i class="fbr-icon-medium uk-icon-triangle-right"></i>',
					156 => '<i class="fbr-icon-medium uk-icon-triangle-up"></i>',
					157 => '<i class="fbr-icon-medium uk-icon-tripadvisor"></i>',
					158 => '<i class="fbr-icon-medium uk-icon-trophy"></i>',
					159 => '<i class="fbr-icon-medium uk-icon-tumblr"></i>',
					160 => '<i class="fbr-icon-medium uk-icon-tv"></i>',
					161 => '<i class="fbr-icon-medium uk-icon-twitter"></i>',
					162 => '<i class="fbr-icon-medium uk-icon-unlock"></i>',
					162 => '<i class="fbr-icon-medium uk-icon-upload"></i>',
					163 => '<i class="fbr-icon-medium uk-icon-user"></i>',
					164 => '<i class="fbr-icon-medium uk-icon-users"></i>',
					165 => '<i class="fbr-icon-medium uk-icon-video-camera"></i>',
					166 => '<i class="fbr-icon-medium uk-icon-vimeo"></i>',
					167 => '<i class="fbr-icon-medium uk-icon-warning"></i>',
					168 => '<i class="fbr-icon-medium uk-icon-whatsapp"></i>',
					169 => '<i class="fbr-icon-medium uk-icon-wine"></i>',
					170 => '<i class="fbr-icon-medium uk-icon-woman"></i>',
					171 => '<i class="fbr-icon-medium uk-icon-world"></i>',
					172 => '<i class="fbr-icon-medium uk-icon-xing"></i>',
					173 => '<i class="fbr-icon-medium uk-icon-youtube"></i>'

    	);
    }

    /**
     *
     * @param string $avalue
     * @return string|unknown
     */
    public static function getBlogFeatureIconHtmlContent($avalue = '')
    {
    	$arr = self::getBlogFeatureIconOptions();
    	return empty($avalue) ? '' : (array_key_exists($avalue, $arr) ? $arr[$avalue] : '' );
    }
    /**
     *
     * @return array[]
     */
    public static function getBlogFeatureHomepageOptions() // getUserProfileStatusOptions()
    {
    	return array(
    			0 => '',
    			1 => Lang::get('fbr.blogfeature::lang.homepage_options.slideshow_heading_excerpt_btn'),
    			2 => Lang::get('fbr.blogfeature::lang.homepage_options.switcher_words'),
    			3 => Lang::get('fbr.blogfeature::lang.homepage_options.icon_title_excerpt_btn'),
    			4 => Lang::get('fbr.blogfeature::lang.homepage_options.alternate_image_title_excerpt_btn'),
    			5 => Lang::get('fbr.blogfeature::lang.homepage_options.panel_without_image'),
    			6 => Lang::get('fbr.blogfeature::lang.homepage_options.figure_title_image'),
    			7 => Lang::get('fbr.blogfeature::lang.homepage_options.panel_text_image'),
    	);
    }

    /**
     * display in the blog post list using
     models/posts/columns.yaml
     public static function getBlogFeatureIconHtmlContent($avalue = '')

    feature_homepage:
        label: fbr.blogfeature::lang.feature_homepage.label
        type: partial
        path: ~/plugins/fbr/blogfeature/models/blogfeature/_content_feature_homepage.htm

     * @param string $avalue
     * @return string|mixed
     */
    public static function getBlogFeatureHomepageHtml($avalue = '')
    {
    	$arr = self::getBlogFeatureHomepage();
    	return empty($avalue) ? '' : (array_key_exists($avalue, $arr) ? $arr[$avalue] : '' );
    }

}
