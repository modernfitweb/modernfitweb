<?php namespace Fbr\BlogFeatures\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFbrBlogfeaturesData extends Migration
{
    public function up()
    {
    	if (!Schema::hasColumn('rainlab_blog_posts', 'feature_icon'))
    	{
        Schema::table('rainlab_blog_posts', function($table)
        {
          if (!Schema::hasColumn('rainlab_blog_posts', 'feature_icon'))
        	{
            $table->tinyInteger('feature_icon')->unsigned()->nullable();
          	$table->tinyInteger('feature_homepage')->unsigned()->defaults(0);
        	}

        });
    	}
    }

    public function down()
    {
        Schema::table('rainlab_blog_posts', function($table)
        {

        	if (Schema::hasColumn('rainlab_blog_posts', 'feature_icon'))
        	{
        		$table->dropColumn([
        				'feature_icon'
        		]);
        	}

        	if (Schema::hasColumn('rainlab_blog_posts', 'feature_homepage'))
        	{
        		$table->dropColumn([
        				'feature_homepage'
        		]);
        	}
        });
    }
}

// ALTER TABLE `system_files` ADD `image_width` INT UNSIGNED NOT NULL AFTER `updated_at`, ADD `image_height` INT UNSIGNED NOT NULL AFTER `image_width`;
