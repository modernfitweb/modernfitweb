<?php namespace AnandPatel\SeoExtension\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Request;
use AnandPatel\SeoExtension\models\Settings;
use URL;

class CmsPage extends ComponentBase
{
    public $page;
    public $seo_title;
    public $seo_description;
    public $seo_keywords;
    public $canonical_url;
    public $redirect_url;
    public $robot_index;
    public $robot_follow;
    public $hasBlog;

    public $ogTitle;
    public $ogUrl;
    public $ogDescription;
    public $ogSiteName;
    public $ogFbAppId;
    public $ogLocale;
    public $ogImage;


    public function componentDetails()
    {
        return [
            'name'        => 'anandpatel.seoextension::lang.component.cms.name',
            'description' => 'anandpatel.seoextension::lang.component.cms.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $theme = Theme::getActiveTheme();
        $page = Page::load($theme,$this->page->baseFileName);
        $this->page["hasBlog"] = false;

        if(!$page->hasComponent("blogPost"))
        {
            // FBR start ///
            
            // original code:
            /*
             $this->seo_title = $this->page["seo_title"] = empty($this->page->meta_title) ? $this->page->title : $this->page->meta_title;
             $this->seo_description = $this->page["seo_description"] = $this->page->meta_description;
             $this->seo_keywords = $this->page["seo_keywords"] = empty($this->page->seo_keywords) ? $this->page->title : $this->page->seo_keywords;
             */
            
            // new code version:
            $getViewBag = $page->getViewBag();
            
            $localeMetaTitle = array_key_exists('localeMetaTitle',$getViewBag->properties)
            ? $getViewBag->properties["localeMetaTitle"] :'';
            ///echo"<pre>";var_dump($getViewBag);echo"</pre>";exit;
            if(is_array($localeMetaTitle) && array_key_exists($this->page->activeLocale, $localeMetaTitle))
            {
                $seo_title = $localeMetaTitle[$this->page->activeLocale];
                $this->seo_title = $this->page["seo_title"] = empty($seo_title) ?  $this->page->meta_title : $seo_title;
            }
            else
            {
                $this->seo_title = $this->page["seo_title"] = empty($this->page->meta_title) ? $this->page->title : $this->page->meta_title;
            }
            
            $localeMetaDescription = array_key_exists('localeMetaDescription',$getViewBag->properties)
            ? $getViewBag->properties["localeMetaDescription"] : '';
            if(is_array($localeMetaDescription)  && array_key_exists($this->page->activeLocale, $localeMetaDescription))
            {
                $seo_title = $localeMetaDescription[$this->page->activeLocale];
                $this->seo_description = $this->page["seo_description"] =
                empty($seo_description) ?  $this->page->meta_description : $seo_description;
            }
            else
            {
                $this->seo_description = $this->page["seo_description"] =
                empty($this->page->meta_description) ? $this->page->title : $this->page->meta_description;
            }
            
            $localeSeoKeywords = array_key_exists('localeSeoKeywords',$getViewBag->properties)
            ? $getViewBag->properties["localeSeoKeywords"] : '';
            if(is_array($localeSeoKeywords)  && array_key_exists($this->page->activeLocale, $localeSeoKeywords))
            {
                $seo_title = $localeSeoKeywords[$this->page->activeLocale];
                $this->seo_keywords = $this->page["seo_keywords"] =
                empty($seo_keywords) ? $this->page->seo_keywords : $seo_keywords;
            }
            else
            {
                $this->seo_keywords = $this->page["seo_keywords"] =
                empty($this->page->seo_keywords) ? $this->page->title : $this->page->seo_keywords;
            }
            //*/
            // FBR end \\\
            $this->canonical_url = $this->page["canonical_url"] = $this->page->canonical_url;
            $this->redirect_url = $this->page["redirect_url"] = $this->page->redirect_url;
            $this->robot_follow = $this->page["robot_follow"] = $this->page->robot_follow;
            $this->robot_index = $this->page["robot_index"] = $this->page->robot_index;

            $settings = Settings::instance();

            if($settings->enable_og_tags)
            {
                $this->ogTitle = empty($this->page->meta_title) ? $this->page->title : $this->page->meta_title;
                $this->ogDescription = $this->page->meta_description;
                $this->ogUrl = empty($this->page->canonical_url) ? Request::url() : $this->page->canonical_url ;
                $this->ogSiteName = $settings->og_sitename;
                $this->ogFbAppId = $settings->og_fb_appid;
            }

        }
        else{
            $this->hasBlog = $this->page["hasBlog"] = true;
        }
    }

}
