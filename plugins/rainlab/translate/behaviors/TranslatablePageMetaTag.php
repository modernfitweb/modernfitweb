<?php namespace RainLab\Translate\Behaviors;

use App;
use RainLab\Translate\Classes\Translator;
use October\Rain\Extension\ExtensionBase;
use ApplicationException;
use Exception;

/**
 * Translatable page URL model extension
 *
 * Usage:
 *
 * In the model class definition:
 *
 *   public $implement = ['@RainLab.Translate.Behaviors.TranslatablePageMetaTag'];
 *
 */
class TranslatablePageMetaTag extends ExtensionBase
{
    /**
     * @var \October\Rain\Database\Model Reference to the extended model.
     */
    protected $model;

    /**
     * @var string Active language for translations.
     */
    protected $translatableContext;

    /**
     * @var string Default system language.
     */
    protected $translatableDefault;

    // FBR start ///
    /**
     * @var string Default page meta_title.
     */
    protected $translatableDefaultMetaTitle;
    
    /**
     * @var string Default page meta_title.
     */
    protected $translatableDefaultMetaDescription;
    /**
     * @var string Default page seo_keywords.
     */
    protected $translatableDefaultSeoKeywords;
    // FBR END ///

    /**
     * Constructor
     * @param \October\Rain\Database\Model $model The extended model.
     */
    public function __construct($model)
    {
        $this->model = $model;

        $this->initTranslatableContext();

        $this->model->bindEvent('model.afterFetch', function() {
            $this->translatableDefaultMetaTitle       = $this->getModelMetaTitle();
            $this->translatableDefaultMetaDescription = $this->getModelMetaDescription();
            $this->translatableDefaultSeoKeywords     = $this->getModelSeoKeywords();
            if (!App::runningInBackend()) {
                $this->rewriteTranslatablePageMetaTitle();
                $this->rewriteTranslatablePageMetaDescription();
                $this->rewriteTranslatablePageSeoKeywords();
            }


        });
    }
    // meta_title
    protected function setModelMetaTitle($value)
    {
        if ($this->model instanceof \RainLab\Pages\Classes\Page) {
            array_set($this->model->attributes, 'viewBag.meta_title', $value);
        }
        else {
            $this->model->meta_title = $value;
        }
    }
    // meta_description
    protected function setModelMetaDescription($value)
    {
        if ($this->model instanceof \RainLab\Pages\Classes\Page) {
            array_set($this->model->attributes, 'viewBag.meta_description', $value);
        }
        else {
            $this->model->meta_description = $value;
        }
    }
    //seo_keywords
    protected function setModelSeoKeywords($value)
    {
        if ($this->model instanceof \RainLab\Pages\Classes\Page) {
            array_set($this->model->attributes, 'viewBag.seo_keywords', $value);
        }
        else {
            $this->model->seo_keywords = $value;
        }
    }


    protected function getModelMetaTitle()
    {
        if ($this->model instanceof \RainLab\Pages\Classes\Page) {
            return array_get($this->model->attributes, 'viewBag.meta_title');
        }
        else {
            return $this->model->meta_title;
        }
    }

    protected function getModelMetaDescription()
    {
        if ($this->model instanceof \RainLab\Pages\Classes\Page) {
            return array_get($this->model->attributes, 'viewBag.meta_description');
        }
        else {
            return $this->model->meta_description;
        }
    }

    protected function getModelSeoKeywords()
    {
        if ($this->model instanceof \RainLab\Pages\Classes\Page) {
            return array_get($this->model->attributes, 'viewBag.seo_keywords');
        }
        else {
            return $this->model->seo_keywords;
        }
    }
    /**
     * Initializes this class, sets the default language code to use.
     * @return void
     */
    public function initTranslatableContext()
    {
        $translate = Translator::instance();
        $this->translatableContext = $translate->getLocale();
        $this->translatableDefault = $translate->getDefaultLocale();
    }

    /**
     * Checks if a translated meta_title exists and rewrites it, this method
     * should only be called from the context of front-end.
     * @return void
     */
    public function rewriteTranslatablePageMetaTitle($locale = null)
    {
        $locale = $locale ?: $this->translatableContext;
        $localeMetaTitle = $this->translatableDefaultMetaTitle;

        if ($locale != $this->translatableDefault) {
            $localeMetaTitle = $this->getSettingsMetaTitleAttributeTranslated($locale) ?: $localeMetaTitle;
        }

        $this->setModelMetaTitle($localeMetaTitle);
    }

    public function rewriteTranslatablePageMetaDescription($locale = null)
    {
        $locale = $locale ?: $this->translatableContext;
        $localeMetaDescription = $this->translatableDefaultMetaDescription;

        if ($locale != $this->translatableDefault) {
            $localeMetaDescription = $this->getSettingsMetaDescriptionAttributeTranslated($locale) ?: $localeMetaDescription;
        }

        $this->setModelMetaDescription($localeMetaDescription);
    }

    public function rewriteTranslatablePageSeoKeywords($locale = null)
    {
        $locale = $locale ?: $this->translatableContext;
        $localeSeoKeywords = $this->translatableDefaultSeoKeywords;

        if ($locale != $this->translatableDefault) {
            $localeSeoKeywords = $this->getSettingsSeoKeywordsAttributeTranslated($locale) ?: $localeSeoKeywords;
        }

        $this->setModelSeoKeywords($localeSeoKeywords);
    }
    /**
     * Determines if a locale has a translated meta_title.
     * @return bool
     */
    public function hasTranslatablePageMetaTitle($locale = null)
    {
        $locale = $locale ?: $this->translatableContext;

        return strlen($this->getSettingsMetaTitleAttributeTranslated($locale)) > 0;
    }

    public function hasTranslatablePageMetaDescription($locale = null)
    {
        $locale = $locale ?: $this->translatableContext;

        return strlen($this->getSettingsMetaDescriptionAttributeTranslated($locale)) > 0;
    }

    public function hasTranslatablePageSeoKeywords($locale = null)
    {
        $locale = $locale ?: $this->translatableContext;

        return strlen($this->getSettingsSeoKeywordsAttributeTranslated($locale)) > 0;
    }
    /**
     * Mutator detected by MLControl
     * @return string
     */
    public function getSettingsMetaTitleAttributeTranslated($locale)
    {
        $defaults = ($locale == $this->translatableDefault) ? $this->translatableDefaultMetaTitle : null;

        return array_get($this->model->attributes, 'viewBag.localeMetaTitle.'.$locale, $defaults);
    }

    public function getSettingsMetaDescriptionAttributeTranslated($locale)
    {
        $defaults = ($locale == $this->translatableDefault) ? $this->translatableDefaultMetaDescription : null;

        return array_get($this->model->attributes, 'viewBag.localeMetaDescription.'.$locale, $defaults);
    }

    public function getSettingsSeoKeywordsAttributeTranslated($locale)
    {
        $defaults = ($locale == $this->translatableDefault) ? $this->translatableDefaultSeoKeywords : null;

        return array_get($this->model->attributes, 'viewBag.localeSeoKeywords.'.$locale, $defaults);
    }
    /**
     * Mutator detected by MLControl
     * @return void
     */
    public function setSettingsMetaTitleAttributeTranslated($value, $locale)
    {
        if ($locale == $this->translatableDefault) {
            return;
        }

        if ($value == $this->translatableDefaultMetaTitle) {
            return;
        }

        /*
         * The CMS controller will purge attributes just before saving, this
         * will ensure the attributes are injected after this logic.
         */
        $this->model->bindEventOnce('model.beforeSave', function() use ($value, $locale) {
            if (!$value) {
                array_forget($this->model->attributes, 'viewBag.localeMetaTitle.'.$locale);
            }
            else {
                array_set($this->model->attributes, 'viewBag.localeMetaTitle.'.$locale, $value);
            }
        });
    }
    public function setSettingsMetaDescriptionAttributeTranslated($value, $locale)
    {
        if ($locale == $this->translatableDefault) {
            return;
        }

        if ($value == $this->translatableDefaultMetaDescription) {
            return;
        }

        /*
         * The CMS controller will purge attributes just before saving, this
         * will ensure the attributes are injected after this logic.
         */
        $this->model->bindEventOnce('model.beforeSave', function() use ($value, $locale) {
            if (!$value) {
                array_forget($this->model->attributes, 'viewBag.localeMetaDescription.'.$locale);
            }
            else {
                array_set($this->model->attributes, 'viewBag.localeMetaDescription.'.$locale, $value);
            }
        });
    }
    public function setSettingsSeoKeywordsAttributeTranslated($value, $locale)
    {
        if ($locale == $this->translatableDefault) {
            return;
        }

        if ($value == $this->translatableDefaultSeoKeywords) {
            return;
        }

        /*
         * The CMS controller will purge attributes just before saving, this
         * will ensure the attributes are injected after this logic.
         */
        $this->model->bindEventOnce('model.beforeSave', function() use ($value, $locale) {
            if (!$value) {
                array_forget($this->model->attributes, 'viewBag.localeSeoKeywords.'.$locale);
            }
            else {
                array_set($this->model->attributes, 'viewBag.localeSeoKeywords.'.$locale, $value);
            }
        });
    }
    /**
     * Mutator detected by MLControl, proxy for Static Pages plugin.
     * @return string
     */
    public function getViewBagMetaTitleAttributeTranslated($locale)
    {
        return $this->getSettingsMetaTitleAttributeTranslated($locale);
    }
    public function getViewBagMetaDescriptionAttributeTranslated($locale)
    {
        return $this->getSettingsMetaDescriptionAttributeTranslated($locale);
    }
    public function getViewBagSeoKeywordsAttributeTranslated($locale)
    {
        return $this->getSettingsSeoKeywordsAttributeTranslated($locale);
    }

    /**
     * Mutator detected by MLControl, proxy for Static Pages plugin.
     * @return void
     */
    public function setViewBagMetaTitleAttributeTranslated($value, $locale)
    {
        $this->setSettingsMetaTitleAttributeTranslated($value, $locale);
    }
    public function setViewBagMetaDescriptionAttributeTranslated($value, $locale)
    {
        $this->setSettingsMetaDescriptionAttributeTranslated($value, $locale);
    }
    public function setViewBagSeoKeywordsAttributeTranslated($value, $locale)
    {
        $this->setSettingsSeoKeywordsAttributeTranslated($value, $locale);
    }
}
